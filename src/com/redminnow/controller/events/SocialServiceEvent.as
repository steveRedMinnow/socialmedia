package com.redminnow.controller.events
{
	import flash.events.Event;

	import mx.collections.ArrayList;
	
	public class SocialServiceEvent extends Event
	{
		public static const AUTHORIZATION_COMPLETE:String = "AUTHORIZATION_COMPLETE";
		public static const GET_POSTS_FOR_USER_COMPLETE:String = "GET_POSTS_FOR_USER_COMPLETE";
		
		public var posts:ArrayList;
		
		public function SocialServiceEvent(type:String)
		{
			super(type, bubbles, cancelable);
		}
	}
}