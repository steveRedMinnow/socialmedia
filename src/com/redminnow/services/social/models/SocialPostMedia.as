package com.redminnow.services.social.models
{
	public class SocialPostMedia
	{
		public var type:String;
		public var url:String;
	}
}