package com.redminnow.services.social.models
{
	import mx.collections.ArrayList;

	public class SocialPost
	{
		public static const TWITTER_SOURCE_TYPE:String = "Twitter";
		
		public var id:String;
		public var sourceType:String;
		public var createdAt:Date;
		public var text:String;
		public var mediaUrls:ArrayList;
		public var userId:String;
		public var userName:String;
		public var userDisplayName:String;
		public var userImageUrl:String;
	}
}