package com.redminnow.services.social.services
{
	import com.hurlant.util.Base64;
	import com.redminnow.controller.events.SocialServiceEvent;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	
	import mx.collections.ArrayList;

	
	public class SocialServiceBase extends EventDispatcher
	{
		protected var authUrl:String;
		protected var apiKey:String;
		protected var apiSecret:String;
		protected var bearerToken:String;
		
		public function SocialServiceBase(authUrl:String, apiKey:String, apiSecret:String)
		{
			this.authUrl = authUrl;
			this.apiKey = apiKey;
			this.apiSecret = apiSecret;
			super();
		}
		
		public function initiateAuthorization():void 
		{
			var request:URLRequest = getOAuthTokenRequest();	
			var loader:URLLoader = getOAuthTokenLoader();
			
			loader.load(request);
		}
		
		protected function setBearerTokenOnRequest(request:URLRequest):void
		{
			var credentialsHeader:URLRequestHeader = new URLRequestHeader("Authorization", "Bearer " + bearerToken);
			request.requestHeaders.push(credentialsHeader);
		}
		
		protected function onIOError(event:IOErrorEvent):void
		{
			dispatchEvent(event);
		}
		
		protected function onSecurityError(event:SecurityErrorEvent):void
		{
			dispatchEvent(event);
		}
		
		protected function sendGetPostsForUserCompleteEvent(posts:ArrayList):void
		{
			var completeEvent:SocialServiceEvent = new SocialServiceEvent(SocialServiceEvent.GET_POSTS_FOR_USER_COMPLETE);
			completeEvent.posts = posts;
			dispatchEvent(completeEvent);
		}
		
		private function getOAuthTokenRequest():URLRequest
		{
			var request:URLRequest = new URLRequest(encodeURI(authUrl));
			request.method = URLRequestMethod.POST;
			
			var keySecret:String = encodeURI(apiKey + ":" + apiSecret);
			
			var credentialsHeader:URLRequestHeader = new URLRequestHeader("Authorization", "Basic " + Base64.encode(keySecret));
			
			request.requestHeaders.push(credentialsHeader);
			request.authenticate = true;
			request.contentType = "application/x-www-form-urlencoded;charset=UTF-8";
			
			return request;
		}
		
		private function getOAuthTokenLoader():URLLoader
		{
			var loader:URLLoader = new URLLoader();
			
			loader.addEventListener(Event.COMPLETE, onInitiateAuthorizationComplete);
			loader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
			loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onSecurityError);
			
			return loader;
		}
		
		private function onInitiateAuthorizationComplete(event:Event):void
		{
			var returnedObject:Object = JSON.parse(event.target.data);
			
			if(returnedObject.hasOwnProperty("access_token"))
			{
				bearerToken = returnedObject.access_token;
				dispatchEvent(new SocialServiceEvent(SocialServiceEvent.AUTHORIZATION_COMPLETE));
			}
		}
		
		
	}
}