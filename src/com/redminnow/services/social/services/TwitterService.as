package com.redminnow.services.social.services
{
	import com.redminnow.services.social.models.SocialPost;
	import com.redminnow.services.social.models.SocialPostMedia;
	
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLVariables;
	
	import mx.collections.ArrayList;


	public class TwitterService extends SocialServiceBase
	{
		private static const TWITTER_AUTH_END_POINT:String = "https://api.twitter.com/oauth2/token?grant_type=client_credentials";
		private static const TWITTER_GET_POSTS_FOR_USER_URL:String = "https://api.twitter.com/1.1/statuses/user_timeline.json";
		
		
		
		public function TwitterService(apiKey:String, apiSecret:String)
		{
			super(TWITTER_AUTH_END_POINT, apiKey, apiSecret);
		}

		public function getPostsForUser(twitterHandle:String, count:int):void
		{
			var request:URLRequest = getGetPostsForUserRequest(twitterHandle, count);	
			var loader:URLLoader = getGetPostsForUserLoader();
			
			loader.load(request);
		}
		
		private function getGetPostsForUserRequest(twitterHandle:String, count:int):URLRequest
		{
			var request:URLRequest = new URLRequest(encodeURI(TWITTER_GET_POSTS_FOR_USER_URL));
			var parameters:URLVariables = new URLVariables();
			
			parameters.screen_name = twitterHandle;
			parameters.count = count;
			
			request.data = parameters;
			
			setBearerTokenOnRequest(request);
			
			return request;
		}
		
		private function getGetPostsForUserLoader():URLLoader
		{
			var loader:URLLoader = new URLLoader();
			
			loader.addEventListener(Event.COMPLETE, onGetPostsForUserComplete);
			loader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
			loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onSecurityError);
			
			return loader;
		}
		
		private function onGetPostsForUserComplete(event:Event):void
		{
			var tweetsRaw:Array = JSON.parse(String(event.currentTarget.data)) as Array;
			var tweets:ArrayList = mapToSocialPosts(tweetsRaw);
			sendGetPostsForUserCompleteEvent(tweets);
		}
		
		private function mapToSocialPosts(tweetsRaw:Array):ArrayList
		{
			var result:ArrayList = new ArrayList();
			
			for each(var tweet:Object in tweetsRaw)
			{
				result.addItem(mapTweetToSocialPost(tweet));
			}
			
			return result;
		}
		
		private function mapTweetToSocialPost(tweet:Object):SocialPost
		{
			var result:SocialPost = new SocialPost();

			result.id = tweet.id_str;
			result.sourceType = SocialPost.TWITTER_SOURCE_TYPE;
			result.createdAt = new Date(Date.parse(tweet.created_at));
			result.text = tweet.text;
			result.mediaUrls = getMediaUrls(tweet.entities.media);
			result.userId = tweet.user.id_str;
			result.userName = tweet.user.screen_name;
			result.userDisplayName = tweet.user.name;
			result.userImageUrl = tweet.user.profile_image_url;
			return result;
		}
		
		private function getMediaUrls(mediaRaw:Array):ArrayList
		{
			var result:ArrayList = new ArrayList();
			
			for each(var media:Object in mediaRaw)
			{
				var socialPostMedia:SocialPostMedia = new SocialPostMedia();
				socialPostMedia.type = media.type;
				socialPostMedia.url = media.url;
				
				result.addItem(socialPostMedia);
			}
			
			return result;
		}
		
	
	}
}