# Red Minnow Social Service Documentation
## Overview
The project is a Flex Library project that outputs a file called **RedMinnowSocialServices.swc** into the bin folder for your use.

## Some Gotchas
* Make sure to call `initiateAuthorization` every hour or so to prevent the authorization token from going stale. 

## Workflow
The basic workflow to retrieve tweets from a specific user is:  
1. Create a new TwitterService with an api key and secret  
2. Listen for `SocialServiceEvent.AUTHORIZATION_COMPLETE`  
3. Call `initiateAuthorization`  
4. After the `SocialServiceEvent.AUTHORIZATION_COMPLETE` event is dispatched, you are good to call any other functions but not before.  

##Authorization Example
```javascript
	
private function setUp():void 
{
	TwitterService service = new TwitterService("someApiKey", "someApiSecret");
	service.addEventListener(SocialServiceEvent.AUTHORIZATION_COMPLETE, serviceOnAuthorizationComplete);  
	service.initiateAuthorization();  
}

private function serviceOnAuthorizationComplete(event:SocialServiceEvent):void
{
	service.addEventListener(SocialServiceEvent.GET_POSTS_FOR_USER_COMPLETE, serviceOnGetPostsForUserComplete);
	service.getPostsForUser
}
```

## getPostsForUser() Example
Assuming authorization is complete.
```javascript
private function setUp():void 
{
	service.addEventListener(SocialServiceEvent.GET_POSTS_FOR_USER_COMPLETE, serviceOnGetPostsForUserComplete);
	service.getPostsForUser("OhioStateFB", 20);  
}

private function serviceOnGetPostsForUserComplete(event:SocialServiceEvent):void
{
	var tweets:ArrayList = event.posts;
	// tweets now has an ArrayList of SocialPost objects in it.
}
```